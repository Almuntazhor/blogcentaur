<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $table = 'posts';
    public $primarykey = 'id';

    protected $fillable = [
    	'id', 'title', 'id_user', 'content'
    ];

    public function user(){

    	return $this->belongsTo('App\model\user','id_user','id');

    }
    public function comment()
    {
    	$this->hasMany('App\model\Comment');
    }

}
