<?php

namespace App\Http\Controllers;

use App\model\Post;
use App\model\User;
use Sentinel;
use App\Http\Requests;
use Centaur\AuthManager;
use Illuminate\Http\Request;

class PostController extends Controller
{
    /** @var Centaur\AuthManager */
    protected $authManager;

    public function __construct(AuthManager $authManager)
    {
        // Middleware
        $this->middleware('sentinel.auth');
        $this->middleware('sentinel.access:posts.create', ['only' => ['create', 'store']]);
        $this->middleware('sentinel.access:posts.view', ['only' => ['index', 'show']]);
        $this->middleware('sentinel.access:posts.update', ['only' => ['edit', 'update']]);
        $this->middleware('sentinel.access:posts.destroy', ['only' => ['destroy']]);

        // Dependency Injection
        $this->authManager = $authManager;
    }

    /**
     * Display a listing of the roles.
     *
     * @return \Illuminate\Http\Response
     */

 	public function index()
    {
        if (Sentinel::inRole('administrator')){
        
        $post = Post::all();
        }else{

        $post = Post::where("id_user", "=", Sentinel::getUser()->id)->get();
        }

        return view('Centaur::posts.index')->with('post',$post);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = User::all();
        
        return view('Centaur::posts.create')->with('user',$user);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    	// Validate the form data
        $result = $this->validate($request, [
            'title' => 'required',
            'id_user' => 'required',
            'image' => 'required',
            'content' => 'required',
        ]);

        $image = $request->file('image');
        $destinationPath = base_path() . '/public/uploads/artikel';
        $image->move($destinationPath, $image->getClientOriginalName());

        $post = Post::create([
                    'image' => $image->getClientOriginalName(),
                    'title' =>$request->input('title'),
                    'id_user' =>$request->input('id_user'),
                    'content' =>$request->input('content')
                ]);
                
        session()->flash('success', "Artikel '{$post->title}' has been created.");
        return redirect()->route('posts.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findorfail($id);     
        $comments = Comment::where('id_post','=',$id)->get();

        return view('admin/home.blog')->with('post',$post)->with('comments',$comments);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);
        $user = User::all();

        return view('Centaur::posts.edit')->with('post',$post)->with('user',$user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
    	// Validate the form data
        $result = $this->validate($request, [
            'title' => 'required',
            'id_user' => 'required',
            'image' => 'required',
            'content' => 'required',
        ]);

        $post = Post::findOrFail($id);
        $image = $request->file('image');
        $destinationPath = base_path() . '/public/uploads/artikel';
        $image->move($destinationPath, $image->getClientOriginalName());

        $post->update([
                    'image' =>$image->getClientOriginalName(),
                    'title' =>$request->input('title'),
                    'id_user' =>$request->input('id_user'),
                    'content' =>$request->input('content')
                ]);
        
        session()->flash('success', "Artikel '{$post->title}' has been Updated.");
        return redirect()->route('posts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        $post->delete();
        return redirect()->route('posts.index');
    }
}