<?php

namespace App\Http\Controllers;

use App\model\Post;
use App\model\Comment;
use Sentinel;
use App\Http\Requests;
use Centaur\AuthManager;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Sentinel::check() && Sentinel::inRole('administrator')){
            
            $post = Post::all();
        }elseif (Sentinel::check() && Sentinel::inRole('moderator')){
            
            $post = Post::where('id_user','=', Sentinel::getUser()->id)->get();
        }else{

            $post = Post::all();
        }

        return view('Centaur::homes.index')->with('post', $post);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findorfail($id);     
        $comments = Comment::where('id_post','=',$id)->get();

        return view('Centaur::homes.home')->with('post',$post)->with('comments',$comments);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
  
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //

        $comment = Post::where('id','=',$id)->delete();
         return redirect('/')->with('message','Success .. ')
                                        ->with('alert','success');
    }
}

