<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;
use Sentinel;

class Post extends Model
{
    protected $table = 'posts';
    public $primarykey = 'id';

    protected $fillable = [
    	'id', 'title', 'id_user', 'content', 'image'
    ];

    public function user(){

    	return $this->belongsTo('App\model\user','id_user','id');
    }
    public function comment()
    {
    	$this->hasMany('App\model\Comment');
    }

}
