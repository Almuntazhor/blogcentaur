<?php

namespace App\model;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = 'users';
    public $primarykey = 'id';
    
    protected $fillable = [
        'email', 'password', 'permissions', 'last_name', 'first_name', 'last_login'
    ];

   public function post(){

        return $this->hasMany('App\model\post','id_user');

    }
}
