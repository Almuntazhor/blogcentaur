<?php

namespace App\model;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $table = 'comments';
    public $primarykey = 'id';
    
    protected $fillable = ['id','id_post','name','email','comment'];

    public function post()
    {
    	$this->belongsTo('App\model\Post','id','id_post');
    }
}
