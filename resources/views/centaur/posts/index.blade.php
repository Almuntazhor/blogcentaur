@extends('Centaur::layout')

@section('title', 'Artikel')

@section('content')
  <div class="row">
    <div class="panel panel-default">
      <div class="panel-body">
            <div class='btn-toolbar pull-right'>
                <a class="btn btn-info" href="{{ route('posts.create') }}">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    Create
                </a>
            </div>
        <h4><i class="glyphicon glyphicon-file"></i>Daftar Artikel</h4><hr>
           
        @if($post->count())
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover table-condensed tfix">
                    <thead align="center">
                       <tr>
                           <td><b>No</b></td>
                           <td><b>Title</b></td>
                           <td><b>Create by</b></td>
                           <td><b>Content</b></td>
                           <td colspan="2"><b>MENU</b></td>
                       </tr>
                   </thead>

                   <p><type="hidden" {{ $no = '1' }}</p>
                   @foreach($post as $val)
                       <tr align="center">
                           <td>{{ $no++ }}</td>
                           <td>{{ $val->title }}</td>
                           <td>{{ $val->user->first_name }}</td>
                           <td>{!! $val->content !!}</td>
                           <td align="center" width="30px">
                               <a href="{{ route('posts.edit', $val->id) }}" class="btn btn-warning btn-sm" role="button"><i class="fa fa-pencil-square"></i> Edit</a>
                           </td>
                           <td align="center" width="30px">
                               {!! Form::open(array(
                                    'route' => array('posts.destroy', $val->id),
                                    'method' => 'delete',
                                    'style' => 'display:inline')) !!}
                                    <button class='btn btn-sm btn-danger delete-btn' type='submit'>
                                        <i class='fa fa-times-circle'></i> Delete
                                    </button>
                                {!! Form::close() !!}
                           </td>
                       </tr>
                   @endforeach
              </table>
          </div>
        @else
            <div class="alert alert-warning">
                <i class="fa fa-exclamation-triangle"></i> Data tidak tersedia
            </div>
        @endif
      </div>
    </div>
  </div>
@stop