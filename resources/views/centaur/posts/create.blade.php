@extends('Centaur::layout')

@section('title', 'Artikel')

@section('content')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<div class="row">
  <h4><i class="glyphicon glyphicon-pencil fa fa-plus-square""></i> Create New Article's</h4><hr>
    <div class="row">
      <div class="col-md-6">
		<div class="panel panel-default">
	  	  <div class="panel-body">
			<form accept-charset="UTF-8" role="form" method="post" enctype="multipart/form-data" action="{{ route('posts.store') }}">
			<fieldset>

			  	<div class="form-group {{ ($errors->has('title')) ? 'has-error' : '' }}">
			   	  	<label for="title" class="control-label">Title</label>
				  	<input type="text" class="form-control" name="title" placeholder="What's your title?" value="{{ old('title') }}">
				  	{!! ($errors->has('title') ? $errors->first('title', '<p class="text-danger">:message</p>') : '') !!}
			 	</div>

				<div class="form-group">
				  	<label for="" class="control-label">Create by:</label>
					<div>
					   	<select name="id_user" class="form-control select">
						@foreach ($user as $data)
							<option value="{{ $data->id }}">{{ $data->first_name }}</option>
						@endforeach
						</select>
					</div>
				</div>

				<div class="form-group {{ ($errors->has('image')) ? 'has-error' : '' }}">
					<label for="" class="control-label">Image</label>
					<div>
						<input type="file" name="image" value="{{ old('image') }}">
						{!! ($errors->has('image') ? $errors->first('image', '<p class="text-danger">:message</p>') : '') !!}
					</div>
				</div>

				<div class="form-group {{ ($errors->has('content')) ? 'has-error' : '' }}">
					<label for="content" class="control-label">Content's</label>
					<textarea class="form-control select" placeholder="Text here.." name="content" id="article-ckeditor" value="{{ old('content') }}">
					</textarea>
					{!! ($errors->has('content') ? $errors->first('content', '<p class="text-danger">:message</p>') : '') !!}
				</div>

				<div class="form-group">
					<input name="_token" value="{{ csrf_token() }}" type="hidden">
					<button type="submit" class="btn btn-info">Save</button>
		            <button type="reset" class="btn btn-danger">Reset</button>
				</div>
			</fieldset>
		  </form>
		</div>
	  </div>
	</div>
  </div>
</div>  
<script>
        CKEDITOR.replace( 'article-ckeditor' );
</script>
@stop