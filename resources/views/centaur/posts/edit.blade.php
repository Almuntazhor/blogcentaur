@extends('Centaur::layout')

@section('title', 'Artikel')

@section('content')
<script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
<div class="row">
  <h4><i class="glyphicon glyphicon-pencil fa fa-plus-square""></i> Create New Article's</h4><hr>
    <div class="row">
      <div class="col-md-6">
		<div class="panel panel-default">
	  	  <div class="panel-body">			
			{!! Form::model($post,['method'=>'PATCH','enctype'=>"multipart/form-data",'action'=>['PostController@update',$post->id]]) !!}
			{{ csrf_field() }}
              <fieldset>
            	<input type="hidden" class="form-control" id="" name="id" placeholder="ID" value="{{ $post->id }}">
				    <div class="form-group {{ ($errors->has('title')) ? 'has-error' : '' }}">
					<label for="title" class="control-label">Title</label>
						<input type="text" class="form-control" id="" name="title" placeholder="What's your title.." value="{{ $post->title }}">
						{!! ($errors->has('title') ? $errors->first('title', '<p class="text-danger">:message</p>') : '') !!}
					</div>

			        <div class="form-group">
					  <label for="" class="control-label">Create by:</label>
						<div>
						  <select name="id_user" class="form-control select" placeholder="Username">
							@foreach ($user as $data)
							<option {{ ($post->id_user == $data->id) ? 'selected="selected"' : '' }} value="{{ $data->id }}">{{ $data->first_name }}</option>
							@endforeach
						  </select>
						</div>
			     	</div>

					<div class="form-group {{ ($errors->has('image')) ? 'has-error' : '' }}">
					  <label for="" class="control-label">Image</label>
						<div>
						  <input type="file" name="image" value="{{ $post->image }}">
						  {{ url('uploads/artikel/'.$post->image) }}
						  {!! ($errors->has('image') ? $errors->first('image', '<p class="text-danger">:message</p>') : '') !!}
						</div>
					</div>

					<div class="form-group {{ ($errors->has('content')) ? 'has-error' : '' }}">
					  <label for="content" class="control-label">Content's</label>
						<textarea class="form-control select" placeholder="Text here.." name="content" id="article-ckeditor">{{ $post->content }}
						</textarea>
						{!! ($errors->has('content') ? $errors->first('content', '<p class="text-danger">:message</p>') : '') !!}
					</div>

					<div class="form-group">

						<input name="_token" value="{{ csrf_token() }}" type="hidden">
						<button type="submit" class="btn btn-info">Save</button>
		                <button type="reset" class="btn btn-danger">Reset</button>
					</div>
			</fieldset>
		  </form>
		</div>
	  </div>
	</div>
  </div>
</div> 
<script>
        CKEDITOR.replace( 'article-ckeditor' );
</script>
@stop
