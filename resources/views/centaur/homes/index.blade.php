@extends('Centaur::layout')

@section('title', 'Home')

@section('content')
@if($post->count())
@foreach ($post as $data)
  <div class="col-xs-12 col-sm-6 col-md-4 col-lg-4">
    <div class="panel panel-default">
      <div class="panel-body">
      @if (Sentinel::check())
        <div class="pull-right">
          <form action="{{ url('posts',$data->id) }}" method="POST">
            {!! csrf_field() !!}
            {!! method_field('DELETE') !!}
              <button class="btn btn-danger" title="DELETE"><i class="fa fa-close"></i></button>
          </form>
         </div>
      @endif
          <h3>
            <a href="{{ route('homes.show', $data->id) }}" title="">{{str_limit($data->title,20)}}</a>
            </h3><br>
          <p>
            <img src='/uploads/artikel/{{ $data->image }}' height="200" width="200px" alt="">
          </p><br>
          <p>{!!str_limit($data->content,100)!!}
            <a href="{{ route('homes.show', $data->id) }}" title="">Read more</a>
          </p>
      </div>
    </div>        
  </div>
@endforeach
@else
    <div class="row">
      <div class="col-lg-12">
      <div class="jumbotron">
            <h1>Nothing Article's!</h1>
            <p>you have a create new article :)</p>
        </div>
      </div>
    </div>
@endif
@stop
