@extends('Centaur::layout')

@section('title', 'Home')

@section('content')
<div class="row">
  <div class="panel panel-info">
    <div class="panel-body">
      <h1>{{$post->title}}</h1>
      <p align="right"><i>
      Date: {{$post->created_at}}
      </i></p>
      <hr>
        <br>
          <p>
            <img src='/uploads/artikel/{{ $post->image }}' height="400" width="500px" alt="">
          </p>
          <p>
            {!!$post->content!!}
          </p>
          <p>
            Create by: <strong><b>{{$post->user->first_name}}</b></strong>
          </p>
          <p>
            E-mail: <strong><b>{{$post->user->email}}</b></strong>
          </p>
      </div>
    </div>

    @if (count($comments)>0)
    <div class="panel panel-default">
      <div class="panel-body">
        <h2>Commented : </h2>
        <br>
          @foreach ($comments as $comment)
            @if (Sentinel::check())
              <div class="pull-right">
                <form action="{{ route('comments.destroy',$comment->id) }}" method="POST">
                  {!! csrf_field() !!}
                  {!! method_field('DELETE') !!}
                  <input type="hidden" name="post_id" value="{{$comment->post_id}}">
                  <button class="btn btn-danger" title="DELETE"><i class="fa fa-close"></i></button>
                </form>
              </div>
             @endif
              <h4><strong>{{$comment->name}}</strong></h4>        
              <p>
                {{$comment->comment}}
              </p>
              <hr>
          @endforeach
      </div>
    </div>
    @endif
   
    <div class="panel panel-default">
      <div class="panel-body">
        <form action="{{route('comments.store')}}" method="POST" role="form">
          {{csrf_field()}}
            <h2>Comment a post</h2>
              <input type="hidden" name="id_post" value="{{$post->id}}">

                <div class="row">
                  <div class="form-group has-info col-md-3">
                    <label for="title">Name</label>
                    <input type="text" class="form-control" name="name" id="title" placeholder="Place Your Name Here">
                  </div>
                </div>

                <div class="row">
                  <div class="form-group has-info col-md-3">
                    <label for="title">E-mail</label>
                    <input type="email" class="form-control" name="email" id="title" placeholder="@example.com">
                  </div>
                </div>

                <div class="row">
                  <div class="form-group has-info col-md-6">
                  <label for="content">Comment</label>
                  <textarea id="content" rows="4" name="comment" placeholder="Place Your Comment Here" class="form-control"></textarea>
                  </div>
                </div> 

          <button type="submit" class="btn btn-info btn-raised">Submit</button>
        </form>
      </div>
    </div>    
  </div>
@stop